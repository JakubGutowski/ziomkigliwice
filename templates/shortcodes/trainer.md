{% set name = name -%}
{% set description = description -%}
{% set img_path = image -%}
{% set img = resize_image(path=img_path, width=150, op="fit_width")%}

### {{name}}

|   |
|:---|
|![{{name}}]({{img.url}})|
|{{description}}|
