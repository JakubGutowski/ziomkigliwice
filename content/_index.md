+++
title = "Legenda ZG"
sort_by = "weight"
+++

# Ziomki Gliwice

Krzewienie deskorolki w Gliwicach, w mieście które miało jeden z pierwszych skateparków w Polsce (1998).
Na którym wyszkolili się: **Tomasz Adamczyk** i **Wojtek Jaworski** którzy byli w teamie **Malita** oraz mieli przejazdy na pierwszym śląskim filmie deskorolkowym **"M.O.T.Y."**. **Mateusz Woźniak** (ZG Reprezentant) **Gold Rookie** Miesiąca, Roku, wyróżniony w **'Być Jak Omar Salazar'**.

Mamy nadzieję doczekać koljnych wymiataczy :bomb:. w tym celu planujemy:  
 *  podzielić się naszym doświadczeniem organizując zajęcia z nauki i doskonalenia jazdy na desce
 *  organizować eventy deskorolkowe takie jak [Pierwszy Gliwicki Jam Deskorolkowy](@/events/first_jam/index.md)
