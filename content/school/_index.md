+++
title = "Szkółka"
sort_by = "weight"
date = "2021/10/14"
draft = true
+++

# Szkółka

**Pierwsza szkółka** w Gliwicach

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Facilisi nullam vehicula ipsum a arcu cursus. A arcu cursus vitae congue mauris rhoncus aenean vel elit. Sed enim ut sem viverra aliquet eget sit amet. Eget felis eget nunc lobortis mattis aliquam faucibus purus in. Porttitor leo a diam sollicitudin tempor id. Non odio euismod lacinia at quis risus sed vulputate. Adipiscing tristique risus nec feugiat in fermentum posuere. Risus nec feugiat in fermentum posuere urna nec tincidunt praesent. At lectus urna duis convallis convallis tellus. Consectetur lorem donec massa sapien faucibus et molestie. Nec ullamcorper sit amet risus nullam eget. Cursus vitae congue mauris rhoncus

# Instruktorzy

{{ trainer(name="Łukasz Wiśniewski", description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Facilisi nullam vehicula ipsum a arcu cursus. A arcu cursus vitae congue mauris rhoncus aenean vel elit. Sed enim ut sem viverra aliquet eget sit amet. Eget felis eget nunc lobortis mattis aliquam faucibus purus in. ", image="wisnia.jpg")}}
{{ trainer(name="Jakub Gutowski", description="Porttitor leo a diam sollicitudin tempor id. Non odio euismod lacinia at quis risus sed vulputate. Adipiscing tristique risus nec feugiat in fermentum posuere. Risus nec feugiat in fermentum posuere urna nec tincidunt praesent. At lectus urna duis convallis convallis tellus. Consectetur lorem donec massa sapien faucibus et molestie. Nec ullamcorper sit amet risus nullam eget. Cursus vitae congue mauris rhoncus.", image="gutek.jpg")}}


# Cennik

|Rodzaj|Czas|Cena|
|:---|:---:|:---:|
|Zajęcia indywidualne| 1h| 99zł|
|Zajęcia grupowe| 1h| 99zł/os.|
