+++
title = "Pierwszy Gliwicki Jam"
template = "post.html"
date = "2021-10-03"
+++

### Pierwszy Gliwicki Jam Deskorolkowy

**Pierwszy Gliwicki Jam Deskorolkowy** - kilka słów już po Evencie.

[SkateEye](https://www.instagram.com/skateeye/) & [StickAll](https://www.stickall.pl) Collabo (**Łukasz Wiśnia:cherries: Wiśniewski** & **Jonasz Maciuch**), czyli jak w kilka tygodni odpalić największe wydarzenie deskorolkowe w mieście!<!-- more -->

Od pomysłu do realizacji. Spotkanie kilku ziomków, nakreślenie planu, podział roboty. I wiecie co? To tylko zarys, to co działo się później to taka ewolucja pierwotnej wersji, że głowa mała. A nie każdy dysponuje czasem na tak szybkie działania jakie musieliśmy podejmować, żeby plan wypalił. Podział obowiązków, Jonasz papierkowa robota, urząd miasta, Wiśnia formuła i wizja zawodów. Promocja w sieci, film, plakat, zaproszenia, sponsorzy, wlepki okolicznościowe i wiele wiele innych. A wszystko gotowe na już. Nie jest to proste i prawdę mówiąc, ostatni tydzień ostro lecieliśmy z tym wszystkim. Odstawiliśmy wszystko inne na bok, całe serce i energia poszły w event. W domu byliśmy tylko fizycznie(żony wybaczcie Nam!), w głowie ciągle zastanawiając się co podkręcić.
I wiecie co? Okazuje się, że mamy niezłe wsparcie, każdy kogo poprosiliśmy o pomoc, dał z siebie ile mógł. Jesteśmy mega wdzięczni każdemu kto w tym czasie Nas wsparł, udostępnił choć jedno info o tym co się szykuje. Bez Waszej pomocy nie byłoby tak grubo🤘

Finał, czyli czas eventu zdecydowanie Nas nie zaskoczył, a po prostu ucieszył! A właśnie dlatego, że tak to miało wyglądać!
Taka miała być frekwencja i tak to widzieliśmy! Widok Was wszystkich, to była najlepsza zapłata za wszystko co włożyliśmy w ten dzień! Formuła Zawodów i połączenia z zabawami dla dzieci była idealna i świetnie to wykorzystaliście. Do tego mnóstwo deskorolkowców, bez których impreza by nie wyszła. Nie byle jakich, bo najlepszych. Choć to chyba każdy kto wpadł zauważył... . Dzięki wszystkim, którzy wpadli, jesteście naj!🤘

Welkie dzięki za pomoc przy organizacji  Jamu dla:
Kuba Gutowski
Patryk Rymsza
Damian Szatan
Mateusz Kubaj
Mateusz Woźniak
Jacek Adamarek
Krzysiek Piotrowski
Damian Chrobot

dla sędziów:
Mateusz Woźniak
Filip Latocha
Krzysiek Piotrowski

Dj:  
{{ ig_url(name="djhopbeat") }}

Fotograf:  
{{ ig_url(name="ad.muzal") }}  
{{ ig_url(name="bryn0l") }}  
{{ ig_url(name="mlodyuroczydzentelmen") }}  

{{ gallery() }}
